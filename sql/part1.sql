-- creating tabbles

use containerdb;

CREATE TABLE `CONTACT` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` TEXT DEFAULT NULL,
  `last_name` TEXT DEFAULT NULL,
  `EMAIL` VARCHAR(255) DEFAULT NULL,
  `phone_number` INT(9) DEFAULT NULL,
  `messege` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) AUTO_INCREMENT=1;

CREATE TABLE `feedback` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `feedback` VARCHAR(255) DEFAULT NULL,
  `nam` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) AUTO_INCREMENT=1;
use containerdb;

CREATE TABLE `SER` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Serv` VARCHAR(255) DEFAULT NULL,
  `descrip` VARCHAR(255) DEFAULT NULL,
  `Price` INT(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) AUTO_INCREMENT=1;

-- adding test data
use containerdb;

INSERT INTO CONTACT (first_name, last_name, EMAIL, phone_number, messege) VALUES ('jared', 'ross', 'jared.ross.nz@gmail.com', '5442114', 'testing table');
INSERT INTO feedback (feedback, nam) VALUES ('the web site could be better', 'jared');
INSERT INTO SER (Serv, descrip, Price) VALUES ('backround art', 'i will coustem make a backround', '25');

-- veiw test tables with data

SELECT * FROM SER;
SELECT * FROM feedback;
SELECT * FROM CONTACT;