<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class Pages extends Controller {

        public function __construct() {
            $this->people = $this->model('People');
        }

        public function index() {

            $this->view('pages/index');
        }

        public function about() {

            $this->view('pages/about');
        }

        public function contact() {

        
            
            $this->view('pages/contact');
        }


            
        public function feedback() {

            $this->view('pages/feedback');
        }
        

        public function news() {

            $this->view('pages/news');
        }

        public function services() {

            
            
            $this->view('pages/services');
        }


        

    }

?>