<?php

    class People {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function title() {
            return "showing content";
        }

        public function getservices() {
            $this->db->query('SELECT * FROM services');
            return $this->db->resultSet();
        }

        public function getSinglePerson($id) {
            $this->db->query('SELECT * FROM tbl_people WHERE ID = :id');
            $this->db->bind(":id", $id);
            return $this->db->resultSet();
        }

        public function addContact($first_name, $last_name, $EMAIL, $phone_number, $message) {

            $this->db->query('INSERT INTO  contact (first_name, last_name, EMAIL, phone_number, message,) VALUES (:fn, :ln, :em :ph :mes)');

            //Convert data for db
            

            $this->db->bind(':fn', $first_name);
            $this->db->bind(':ln', $last_name);
            $this->db->bind(':em', $EMAIL);
            $this->db->bind(':ph', $phone_number);
            $this->db->bind(':mes', $message);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }

        }

       
    }

?>